


TYPE_FOLDER = 'types'
FOLDER = "./SINQ_LAISON_STATS/"
TIME_FORMAT = '%Y-%m-%dT%H:%M:%S'
SKIP_OLDER_THAN_N_DAYS = 15

DODIXIE = 60011866

import os
import json
import csv
import unicodedata
from datetime import datetime as dt


def get_data_for_type(data_in_json):
    now = dt.now()
    daily = {
        'volume': 0,
        'money': 0,
        'days': 0,
    }

    for h in data_in_json['history']:
        if (now - dt.strptime(h['date'], TIME_FORMAT)).days > SKIP_OLDER_THAN_N_DAYS:
            continue
        daily['volume'] += h['volume']
        daily['money'] += h['volume'] * h["avgPrice"] 
        daily['days'] += 1

    results = {
        'name': unicodedata.normalize('NFKD', data_in_json['type']['type']['name']).encode('ascii','ignore'),
        'id': data_in_json['type']['type']['id'],
        'perdayvolume': 0,
        'perdaymoney': 0,
    }

    if daily['days']:
        results['perdayvolume'] = daily['volume'] / daily['days']
        results['perdaymoney'] = daily['money'] / daily['days']

    all_buys = [b['price'] for b in data_in_json['buys'] if b['location']['id'] == DODIXIE]
    all_sells = [s['price'] for s in data_in_json['sells'] if s['location']['id'] == DODIXIE]

    results.update({
        'buyorders': len(all_buys),
        'maxbuy': max(all_buys or (9999999999999999999.0,)),
        'sellorders': len(all_sells),
        'minsell': min(all_sells or (0.00000000000001,)),
    })

    results.update({
        'margin': results['minsell'] / results['maxbuy'],
        'profitmargin': results['minsell'] - results['maxbuy'],
    })

    results['perdayprofit'] = results['perdayvolume'] * results['profitmargin']
    return results

def analyse():
    with open(os.path.join(FOLDER, 'analysis.csv'), 'w') as csvfile:
        fieldnames = [
            'name',
            'id',
            'perdayvolume',
            'perdaymoney',
            'buyorders',
            'maxbuy',
            'sellorders',
            'minsell',
            'margin',
            'profitmargin',
            'perdayprofit',
        ]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()

        folder = os.path.join(FOLDER, TYPE_FOLDER)
        for jsonfile in os.listdir(folder):
            data_in_json = json.load(open(os.path.join(folder,jsonfile)))
            data = get_data_for_type(data_in_json)
            print data
            writer.writerow(data)

if __name__ == '__main__':
    analyse()
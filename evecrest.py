import requests
import concurrent.futures
import asyncio
import queue

ROOT_URL = 'https://crest-tq.eveonline.com'

TYPE_URL = '?type=https://crest-tq.eveonline.com/inventory/types/{type_id}'

# https://crest-tq.eveonline.com/market/types/
MARKET_TYPES = '/market/types/'
# https://crest-tq.eveonline.com/inventory/types/32772/
MARKET_TYPE = '/market/types/{type_id}/'
# https://crest-tq.eveonline.com/market/prices/
MARKET_PRICES = '/market/prices/'
# https://crest-tq.eveonline.com/market/10000002/orders/sell/?type=https://crest-tq.eveonline.com/inventory/types/34/
SELL_OR_BUY = '/market/{regionId}/orders/{sell_or_buy}/{typeurl}/'
# https://crest-tq.eveonline.com/market/10000002/orders/all/
ALL_ORDERS = '/market/{regionId}/orders/{all_or_typeurl}/'
# https://crest-tq.eveonline.com/market/10000002/history/?type=https://crest-tq.eveonline.com/inventory/types/34/
MARKET_HISTORY = '/market/{regionId}/history/{typeurl}/'

MAX_REQUESTS_PER_SECOND = 100

MAX_WORKERS = 100

_loop = None
_request_times = []


def get_loop():
    return _loop


async def update_request_times():
    global _request_times
    while True:
        curtime = _loop.time()
        while _request_times and _request_times[0] + 1 < curtime:
            _request_times = _request_times[1:]

        if len(_request_times) > MAX_REQUESTS_PER_SECOND:
            await asyncio.sleep((_request_times[0] + 1) - curtime, loop=_loop)
        else:
            _request_times.append(curtime)
            return


async def do_request(reqpath, parse_json=True):
    await update_request_times()
    r = await _loop.run_in_executor(None, requests.get, ROOT_URL + reqpath)
    return r.json() if parse_json else r.text


def sync_save_to_file(path, data):
    with open(path, 'w') as f:
        f.write(data)


async def save_to_file(path, data):
    await _loop.run_in_executor(None, sync_save_to_file, path, data)


async def do_request_and_save(reqpath, path):
    data = await do_request(reqpath, False)
    await save_to_file(path, data)


def do(func):
    global _loop
    _loop = asyncio.get_event_loop()
    executor = concurrent.futures.ThreadPoolExecutor(max_workers=MAX_WORKERS)
    _loop.set_default_executor(executor)
    _loop.run_until_complete(func())
    _loop.close()

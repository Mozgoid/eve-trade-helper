
# DOCS:
# https://eveonline-third-party-documentation.readthedocs.io/en/latest/sso/authentication.html

import urllib
import urlparse

CLIENT_ID = 'e1ac0edae63b44f19a1bd9ea911c9d7a'
CLIENT_SICRET = 'TbdpOKsQA8yWSddSL2xnbn8CSPTpplsy5vs8rOg3'
PORT = 40666

PARAMS = {
    'response_type': 'code',
    'redirect_uri': 'http://localhost:' + str(PORT),
    'client_id': CLIENT_ID,
    'scope': 'characterMailRead characterMarketOrdersRead characterWalletRead',
    'state': 'uniquestate123',
}


import webbrowser
loginurl = 'https://login.eveonline.com/oauth/authorize?' + urllib.urlencode(PARAMS)
webbrowser.open(loginurl)


import SocketServer
import SimpleHTTPServer
class LoginCodeGetter(SimpleHTTPServer.SimpleHTTPRequestHandler):
    code = None
    def do_GET(self):
        parsedParams = urlparse.urlparse(self.path)
        queryParsed = urlparse.parse_qs(parsedParams.query)
        global code
        LoginCodeGetter.code = queryParsed['code'][0]
        self.send_response(200)
        self.send_header('Content-Type', 'text/plain')
        self.end_headers()
        self.wfile.write('code is: ' + LoginCodeGetter.code);
        self.wfile.close();


# run local server until we get code
httpd = SocketServer.TCPServer(('', PORT), LoginCodeGetter)
while not LoginCodeGetter.code:
    httpd.handle_request()


import requests
import base64

headers = {
    "Authorization": "Basic " + base64.b64encode('{}:{}'.format(CLIENT_ID, CLIENT_SICRET)),
    'Content-Type': 'application/x-www-form-urlencoded',
    "Host": "login.eveonline.com",
}
data = 'grant_type=authorization_code&code=' + LoginCodeGetter.code

r = requests.post('https://login.eveonline.com/oauth/token', headers=headers, data=data)
access_token = r.json()['access_token']

# https://api.eveonline.com/char/AccountBalance.xml.aspx?characterID=93265215&accessToken=Ls-vx-xMqq03cl7yMPWdWTVyp1TQUod7WyXti_EmuEVaUMt5JQibftknW06mtFDfhLS7khMP7S93QxXeUzGnxw2

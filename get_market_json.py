import evecrest
import os
import json

SINQ_LAISON = 10000032
FORGE = 10000002

DODIXIE = 60011866

TYPE_JSON_PATH = '{type_id}.json'
TYPE_FOLDER = 'types'
REGION = SINQ_LAISON
FOLDER = "./SINQ_LAISON_STATS/"


async def collect_stats():
    loop = evecrest.get_loop()
    t = loop.create_task(evecrest.do_request_and_save(
        evecrest.MARKET_TYPES, os.path.join(FOLDER, 'market_types.json')))
    p = loop.create_task(evecrest.do_request_and_save(
        evecrest.MARKET_PRICES, os.path.join(FOLDER, 'market_prices.json')))
    a = loop.create_task(evecrest.do_request_and_save(
        evecrest.ALL_ORDERS.format(regionId=REGION, all_or_typeurl="all"),
        os.path.join(FOLDER, 'all_orders.json')))
    await t
    await p
    await a


async def get_and_save_info_for_type(item_info, infostr):
    type_id = item_info['type']['id']
    typeurl = evecrest.TYPE_URL.format(type_id=type_id)

    loop = evecrest.get_loop()

    s = loop.create_task(
        evecrest.do_request(
            evecrest.SELL_OR_BUY.format(
                regionId=REGION, sell_or_buy="sell", typeurl=typeurl)))
    b = loop.create_task(
        evecrest.do_request(
            evecrest.SELL_OR_BUY.format(
                regionId=REGION, sell_or_buy="buy", typeurl=typeurl)))
    h = loop.create_task(
        evecrest.do_request(
            evecrest.MARKET_HISTORY.format(
                regionId=REGION, typeurl=typeurl)))

    sells = await s
    buys = await b
    history = await h

    print(infostr)

    jsonname = TYPE_JSON_PATH.format(type_id=type_id)
    jsonpath = os.path.join(FOLDER, TYPE_FOLDER, jsonname)
    result = {
        'type': item_info,
        'sells': sells.get('items', []),
        'buys': buys.get('items', []),
        'history': history.get('items', []),
    }
    jsondata = json.dumps(result, indent=4)
    await evecrest.save_to_file(jsonpath, jsondata)


async def save_all_market_states():
    prices = await evecrest.do_request(evecrest.MARKET_PRICES)
    itemcount = len(prices['items'])
    i = 0
    tasks = []
    loop = evecrest.get_loop()
    for price in prices['items'][:1000]:
        i += 1
        infostr = '{}/{} type:{}, {}'.format(
            i, itemcount, price['type']['id'], price['type']['name'])
        t = loop.create_task(get_and_save_info_for_type(price, infostr))
        tasks.append(t)

    for t in tasks:
        try:
            await t
        except Exception as e:
            import traceback
            traceback.print_exc()

if __name__ == '__main__':
    evecrest.do(save_all_market_states)
